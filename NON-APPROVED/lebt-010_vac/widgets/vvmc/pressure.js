PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var tooltip  = "N/A";

function log_pv(pv) {
	try {
		Logger.info(pv + ": " + PVUtil.getString(pv));
	} catch (err) {
		Logger.severe(err);
	}
}

try {
	var pvStat   = PVUtil.getString(pvs[0]);
	var pvPrsStr = PVUtil.getString(pvs[2]);

	log_pv(pvs[0]);
	log_pv(pvs[1]);
	log_pv(pvs[2]);

	if (pvStat == "ON") {
		try {
			var vtype = PVUtil.getVType(pvs[1]);

			FormatOption = org.csstudio.display.builder.model.properties.FormatOption;
			FormatOptionHandler = org.csstudio.display.builder.model.util.FormatOptionHandler;

			tooltip = FormatOptionHandler.format(vtype, FormatOption.EXPONENTIAL, 3, true);
		} catch (err) {
			var pvUnit   = PVUtil.getString(pvs[3]);
			tooltip = pvPrsStr + " " + pvUnit;
		}
	} else
		tooltip = pvPrsStr;
} catch (err) {
	Logger.severe(err);
}

widget.setPropertyValue("tooltip", tooltip);
