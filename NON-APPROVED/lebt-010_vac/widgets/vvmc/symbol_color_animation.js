PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var pvMode         = "";
var pvInRange      = 0;
var pvRangeTimeout = 0;

var pvSymbol = pvs[0];

var colorID  = 0;

function log_pv(pv) {
	Logger.info(pv + ": " + PVUtil.getString(pv));
}

try {
	pvStat         = PVUtil.getString(pvs[1]);
	pvMode         = PVUtil.getString(pvs[2]);
	pvInRange      = PVUtil.getInt(pvs[3]);
	pvRangeTimeout = PVUtil.getInt(pvs[4]);

	log_pv(pvs[1]);
	log_pv(pvs[2]);
	log_pv(pvs[3]);
	log_pv(pvs[4]);

	if (pvStat == "ON") {
		switch (pvMode) {
			case "Open":
				Logger.info(pvSymbol + ": Open");
				colorID = 1;
				break;

			case "Closed":
				Logger.info(pvSymbol + ": Closed");
				colorID = 2;
				break;

			case "Setpoint":
				Logger.info(pvSymbol + ": Setpoint");
				if (pvRangeTimeout) {
					Logger.info(pvSymbol + ": Flow range timeout");
					colorID = 3;
				} else if (pvInRange) {
					Logger.info(pvSymbol + ": Flow in range");
					colorID = 1;
				} else {
					Logger.info(pvSymbol + ": Flow not yet in range");
					colorID = 0;
				}

				break;

			case "PID Ctrl":
			case "Ratio A":
			case "Ratio M":
				Logger.info(pvSymbol + ": NO-COLOR-CODES-FOR-STATE");
				colorID = 0;
				break;

			default:
				Logger.severe(pvSymbol + ": Unknown mode:" + pvMode);
				break;
		}
	} else if (pvStat == "ERROR") {
		colorID = 3;
	}
} catch (err) {
	Logger.severe("NO CONNECTION: " + err);
}


pvSymbol.write(colorID);
