PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var errorMsg  = "";
var errorCode = 0;

if (PVUtil.getLong(pvs[0]))
	errorCode = PVUtil.getLong(pvs[1]);

switch (errorCode) {
	case 99:
		errorMsg = "Controller Error (Hardware Error)";
		break;
	case 98:
		errorMsg = "Pressure Interlock";
		break;
	case 97:
		errorMsg = "Hardware Interlock";
		break;
	case 96:
		errorMsg = "Software Interlock";
		break;
	case 49:
		errorMsg = "Controller Error (Hardware Error) - Auto Reset";
		break;
	case 48:
		errorMsg = "Pressure Interlock - Auto Reset";
		break;
	case 47:
		errorMsg = "Hardware Interlock - Auto Reset";
		break;
	case 46:
		errorMsg = "Software Interlock - Auto Reset";
		break;

	case 0:
		break;
	default:
		errorMsg = "Error Code: " + PVUtil.getString(pvs[1]);
		break;
}
Logger.info(pvs[1] + " Error code: " + errorCode + " message: " + errorMsg);

try {
	pvs[2].setValue(errorMsg);
} catch (err) {
	Logger.warning(err);
}
