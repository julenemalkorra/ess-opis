PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var pvHealthy   = 0;
var pvTripped   = 0;
var pvOverriden = 0;
var pvDisabled  = 0;

var pvSymbol    = pvs[0];


var sum     = 0;
var isValid = 0;
var visible = true;
var colorID = 0;

function log_pv(pv) {
	Logger.info(pv + ": " + PVUtil.getString(pv));
}


try {
	pvHealthy   = 1 * PVUtil.getInt(pvs[1]);
	pvTripped   = 2 * PVUtil.getInt(pvs[2]);
	pvOverriden = 4 * PVUtil.getInt(pvs[3]);
	pvDisabled  = 1 * PVUtil.getInt(pvs[4]);

	log_pv(pvs[1]);
	log_pv(pvs[2]);
	log_pv(pvs[3]);
	log_pv(pvs[4]);

	sum         = pvHealthy | pvTripped | pvOverriden | pvDisabled;
	isValid     = (sum & (sum - 1)) == 0 ? 1 : 0;

	if (isValid == 0) {
		Logger.severe(pvSymbol + ": Invalid combination");
	} else if (pvTripped) {
		Logger.info(pvSymbol + ": TRIPPED");
		colorID = 2;
	} else if (pvOverriden) {
		Logger.info(pvSymbol + ": OVERRIDEN");
		colorID = 3;
	} else if (pvDisabled) {
		Logger.info(pvSymbol + ": DISABLED");
		colorID = 4;
		visible = false;
	} else if (pvHealthy) {
		Logger.info(pvSymbol + ": HEALTHY");
		colorID = 1;
	} else {
		Logger.severe(pvSymbol + ": Unknown combination:" + sum);
	}
} catch (err) {
	Logger.severe("NO CONNECTION: " + err);
}

pvSymbol.write(colorID);
widget.setPropertyValue("visible", visible);
