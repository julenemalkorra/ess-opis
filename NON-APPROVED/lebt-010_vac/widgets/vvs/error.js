PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var errorMsg  = "";
var errorCode = 0;

if (PVUtil.getLong(pvs[0]))
	errorCode = PVUtil.getLong(pvs[1]);

switch (errorCode) {
	case 99:
		errorMsg = "Open / On Status & Close / Off Status Both Active";
		break;
	case 49:
		errorMsg = "Previous Interlock";
		break;
	case 48:
		errorMsg = "Next Interlock";
		break;
	case 47:
		errorMsg = "Personnel Access Interlock";
		break;

	case 0:
		break;
	default:
		errorMsg = "Error Code: " + PVUtil.getString(pvs[1]);
		break;
}
Logger.info(pvs[1] + " Error code: " + errorCode + " message: " + errorMsg);

try {
	pvs[2].setValue(errorMsg);
} catch (err) {
	Logger.warning(err);
}
