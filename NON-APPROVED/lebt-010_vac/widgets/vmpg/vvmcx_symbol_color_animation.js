PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var pvOpen      = 0;

var pvSymbol    = pvs[0];

var colorID = 0;

function log_pv(pv) {
	Logger.info(pv + ": " + PVUtil.getString(pv));
}

try {
	pvOpen      = PVUtil.getInt(pvs[1]);

	log_pv(pvs[1]);

	if (pvOpen) {
		Logger.info(pvSymbol + ": OPEN");
		colorID = 1;
	} else {
		Logger.info(pvSymbol + ": CLOSED");
		colorID = 2;
	}
} catch (err) {
	Logger.severe("NO CONNECTION: " + err);
}


pvSymbol.write(colorID);
