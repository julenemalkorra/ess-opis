from org.csstudio.display.builder.runtime.script import PVUtil
from org.csstudio.display.builder.runtime.script import ScriptUtil

Logger = ScriptUtil.getLogger()

pvSymbol  = pvs[0]

colorID   = 0
tooltip   = "N/A"
interlock = "Pressure" if "Prs" in str(pvs[0]) else "Pump"

def log_pv(pv):
	Logger.info(str(pv) + ": " + PVUtil.getString(pv))


try:
	pvHealthy   = PVUtil.getInt(pvs[1])
	log_pv(pvs[1])

	try:
		pvOverridden = PVUtil.getInt(pvs[2])
		log_pv(pvs[2])
	except:
# no Overriden pv
		pvOverridden = 0

	if pvOverridden:
		Logger.info(str(pvSymbol) + ": OVERRIDDEN")
		tooltip = "OVERRIDDEN"
		colorID = 3
	elif pvHealthy:
		Logger.info(str(pvSymbol) + ": HEALTHY")
		tooltip = "HEALTHY"
		colorID = 2
	else:
		Logger.info(str(pvSymbol) + ": TRIPPED")
		tooltip = "TRIPPED"
		colorID = 1
except Exception as err:
	Logger.severe("NO CONNECTION: " + str(err))

pvSymbol.write(colorID)
widget.setPropertyValue("tooltip", interlock + " interlock is: " + tooltip)
