PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var errorMsg  = "";
var errorCode = 0;

if (PVUtil.getLong(pvs[0]))
	errorCode = PVUtil.getLong(pvs[1]);

switch (errorCode) {
	case 99:
		errorMsg = "Open / On Status & Closed / Off Status Both Active";
		break;
	case 98:
		errorMsg = "Valve Not Closed";
		break;
	case 97:
		errorMsg = "Pressure Interlock No. 1";
		break;
	case 96:
		errorMsg = "Hardware Interlock No. 1";
		break;
	case 95:
		errorMsg = "Pressure Interlock No. 2";
		break;
	case 94:
		errorMsg = "Hardware Interlock No. 2";
		break;
	case 93:
		errorMsg = "Hardware Interlock No. 3";
		break;
	case 92:
		errorMsg = 'Software Interlock (From "External" PLC Function)';
		break;
	case 49:
		errorMsg = "Pressure Interlock No. 1 - Auto Reset";
		break;
	case 48:
		errorMsg = "Hardware Interlock No. 1 - Auto Reset";
		break;
	case 47:
		errorMsg = "Pressure Interlock No. 2 - Auto Reset";
		break;
	case 46:
		errorMsg = "Hardware Interlock No. 2 - Auto Reset";
		break;
	case 45:
		errorMsg = "Hardware Interlock No. 3 - Auto Reset";
		break;
	case 44:
		errorMsg = "Software Interlock - Auto Reset";
		break;

	case 0:
		break;
	default:
		errorMsg = "Error Code: " + PVUtil.getString(pvs[1]);
		break;
}
Logger.info(pvs[1] + " Error code: " + errorCode + " message: " + errorMsg);

try {
	pvs[2].setValue(errorMsg);
} catch (err) {
	Logger.warning(err);
}
