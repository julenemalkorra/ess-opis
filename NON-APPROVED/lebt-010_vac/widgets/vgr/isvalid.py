from org.csstudio.display.builder.runtime.script import PVUtil
from org.csstudio.display.builder.model.properties import NamedWidgetColor
from org.csstudio.display.builder.model.persist import WidgetColorService

color = WidgetColorService.getColor("INVALID")
try:
	if PVUtil.getSeverity(pvs[0]) == 0 and PVUtil.getString(pvs[0]).startswith('"HAL RC RGA 101'):
		color = WidgetColorService.getColor("Background")
except:
	pass

widget.setPropertyValue('background_color', color)
