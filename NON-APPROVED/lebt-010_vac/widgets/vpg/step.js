PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var StepAlias = "";
var StepCode  = 0;

if (PVUtil.getLong(pvs[0]))
	StepCode = PVUtil.getLong(pvs[1]);

switch (StepCode) {
	case 1:
		StepAlias = "VPG OFF (Init Step)";
		break;
	case 2:
		StepAlias = "Starting Low Vacuum";
		break;
	case 3:
		StepAlias = "Low Vacuum Devices Locked";
		break;
	case 4:
		StepAlias = "Primary Pump Starting";
		break;
	case 5:
		StepAlias = "Primary Pump Accelerating";
		break;
	case 6:
		StepAlias = "Low Vacuum Valve Opening";
		break;
	case 7:
		StepAlias = "Low Vacuum Pumping On-Going";
		break;
	case 8:
		StepAlias = "Not Used";
		break;
	case 9:
		StepAlias = "Not Used";
		break;
	case 10:
		StepAlias = "Starting High Vacuum";
		break;
	case 11:
		StepAlias = "High Vacuum Devices Locked";
		break;
	case 12:
		StepAlias = "Starting Delay";
		break;
	case 13:
		StepAlias = "High Vacuum Pump Starting";
		break;
	case 14:
		StepAlias = "High Vacuum Pump Accelerating";
		break;
	case 15:
		StepAlias = "High Vacuum Valve Opening";
		break;
	case 16:
		StepAlias = "High Vacuum Pumping On-Going";
		break;
	case 17:
		StepAlias = "High Vacuum Pumping Error";
		break;
	case 18:
		StepAlias = "Not Used";
		break;
	case 19:
		StepAlias = "Not Used";
		break;
	case 20:
		StepAlias = "Not Used";
		break;
	case 21:
		StepAlias = "High Vacuum Devices Locked";
		break;
	case 22:
		StepAlias = "Starting Delay";
		break;
	case 23:
		StepAlias = "High Vacuum Pump Starting";
		break;
	case 24:
		StepAlias = "High Vacuum Pump Accelerating";
		break;
	case 25:
		StepAlias = "High Vacuum Valve Opening";
		break;
	case 26:
		StepAlias = "High Vacuum Pumping On-Going";
		break;
	case 27:
		StepAlias = "High Vacuum Pumping Error";
		break;
	case 28:
		StepAlias = "Not Used";
		break;
	case 29:
		StepAlias = "Not Used";
		break;
	case 30:
		StepAlias = "Not Used";
		break;
	case 31:
		StepAlias = "High Vacuum Devices Locked";
		break;
	case 32:
		StepAlias = "Starting Delay";
		break;
	case 33:
		StepAlias = "High Vacuum Pump Starting";
		break;
	case 34:
		StepAlias = "High Vacuum Pump Accelerating";
		break;
	case 35:
		StepAlias = "High Vacuum Valve Opening";
		break;
	case 36:
		StepAlias = "High Vacuum Pumping On-Going";
		break;
	case 37:
		StepAlias = "High Vacuum Pumping Error";
		break;
	case 38:
		StepAlias = "Not Used";
		break;
	case 39:
		StepAlias = "Not Used";
		break;
	case 40:
		StepAlias = "Not Used";
		break;
	case 41:
		StepAlias = "High Vacuum Devices Locked";
		break;
	case 42:
		StepAlias = "Starting Delay";
		break;
	case 43:
		StepAlias = "High Vacuum Pump Starting";
		break;
	case 44:
		StepAlias = "High Vacuum Pump Accelerating";
		break;
	case 45:
		StepAlias = "High Vacuum Valve Opening";
		break;
	case 46:
		StepAlias = "High Vacuum Pumping On-Going";
		break;
	case 47:
		StepAlias = "High Vacuum Pumping Error";
		break;
	case 48:
		StepAlias = "Not Used";
		break;
	case 49:
		StepAlias = "Not Used";
		break;
	case 50:
		StepAlias = "Not Used";
		break;
	case 51:
		StepAlias = "High Vacuum Devices Locked";
		break;
	case 52:
		StepAlias = "Starting Delay";
		break;
	case 53:
		StepAlias = "Starting High Vacuum Pump";
		break;
	case 54:
		StepAlias = "High Vacuum Pump Accelerating";
		break;
	case 55:
		StepAlias = "High Vacuum Valve Opening";
		break;
	case 56:
		StepAlias = "High Vacuum Pumping On-Going";
		break;
	case 57:
		StepAlias = "High Vacuum Pumping Error";
		break;
	case 58:
		StepAlias = "Not Used";
		break;
	case 59:
		StepAlias = "Not Used";
		break;
	case 60:
		StepAlias = "Not Used";
		break;
	case 61:
		StepAlias = "High Vacuum Pumping Transition";
		break;
	case 62:
		StepAlias = "Nominal";
		break;
	case 63:
		StepAlias = "Not Used";
		break;
	case 64:
		StepAlias = "Not Used";
		break;
	case 65:
		StepAlias = "Stopping High Vacuum Pumping";
		break;
	case 66:
		StepAlias = "High Vacuum Devices Locked";
		break;
	case 67:
		StepAlias = "Valve Closing";
		break;
	case 68:
		StepAlias = "High Vacuum Pump Stopping";
		break;
	case 69:
		StepAlias = "Valve Closing";
		break;
	case 70:
		StepAlias = "High Vacuum Devices Locked";
		break;
	case 71:
		StepAlias = "Valve Closing";
		break;
	case 72:
		StepAlias = "High Vacuum Pump Stopping";
		break;
	case 73:
		StepAlias = "Valve Closing";
		break;
	case 74:
		StepAlias = "High Vacuum Devices Locked";
		break;
	case 75:
		StepAlias = "Valve Closing";
		break;
	case 76:
		StepAlias = "High Vacuum Pump Stopping";
		break;
	case 77:
		StepAlias = "Valve Closing";
		break;
	case 78:
		StepAlias = "High Vacuum Devices Locked";
		break;
	case 79:
		StepAlias = "Valve Closing";
		break;
	case 80:
		StepAlias = "High Vacuum Pump Stopping";
		break;
	case 81:
		StepAlias = "Valve Closing";
		break;
	case 82:
		StepAlias = "High Vacuum Devices Locked";
		break;
	case 83:
		StepAlias = "Valve Closing";
		break;
	case 84:
		StepAlias = "High Vacuum Pump Stopping";
		break;
	case 85:
		StepAlias = "Valve Closing";
		break;
	case 86:
		StepAlias = "Stopping Low Vacuum Pumping";
		break;
	case 87:
		StepAlias = "Valve Closing";
		break;
	case 88:
		StepAlias = "Low Vacuum Pump Stopping";
		break;
	case 89:
		StepAlias = "Not Used";
		break;
	case 90:
		StepAlias = "Not Used";
		break;
	case 91:
		StepAlias = "Not Used";
		break;
	case 92:
		StepAlias = "Not Used";
		break;
	case 93:
		StepAlias = "Not Used";
		break;
	case 94:
		StepAlias = "Turbo-Pumps Venting - Delay";
		break;
	case 95:
		StepAlias = "Turbo Pumps Venting";
		break;
	case 96:
		StepAlias = "Turbo Pumps Venting - Valves Closing";
		break;
	case 97:
		StepAlias = "Not Used";
		break;
	case 98:
		StepAlias = "Not Used";
		break;
	case 99:
		StepAlias = "Not Used";
		break;
	case 100:
		StepAlias = "Not Used";
		break;
	case 101:
		StepAlias = "Not Used";
		break;
	case 102:
		StepAlias = "Valve's Interlocks Overriding";
		break;
	case 103:
		StepAlias = "Valves Opening";
		break;
	case 104:
		StepAlias = "Starting Low Vacuum";
		break;
	case 105:
		StepAlias = "Low Vacuum Devices Locked";
		break;
	case 106:
		StepAlias = "Primary Pump Starting";
		break;
	case 107:
		StepAlias = "Primary Pump Accelerating";
		break;
	case 108:
		StepAlias = "Low Vacuum Valve Opening";
		break;
	case 109:
		StepAlias = "Low Vacuum Pumping On-Going";
		break;
	case 110:
		StepAlias = "Starting High Vacuum";
		break;
	case 111:
		StepAlias = "High Vacuum Devices Locked";
		break;
	case 112:
		StepAlias = "High Vacuum Pump Starting";
		break;
	case 113:
		StepAlias = "High Vacuum Pump Accelerating";
		break;
	case 114:
		StepAlias = "High Vacuum Pumping Delay";
		break;
	case 115:
		StepAlias = "High Vacuum Pump Error";
		break;
	case 116:
		StepAlias = "Not Used";
		break;
	case 117:
		StepAlias = "Not Used";
		break;
	case 118:
		StepAlias = "Not Used";
		break;
	case 119:
		StepAlias = "Not Used";
		break;
	case 120:
		StepAlias = "Not Used";
		break;
	case 121:
		StepAlias = "High Vacuum Devices Locked";
		break;
	case 122:
		StepAlias = "High Vacuum Pump Starting";
		break;
	case 123:
		StepAlias = "High Vacuum Pump Accelerating";
		break;
	case 124:
		StepAlias = "High Vacuum Pumping Delay";
		break;
	case 125:
		StepAlias = "High Vacuum Pump Error";
		break;
	case 126:
		StepAlias = "Not Used";
		break;
	case 127:
		StepAlias = "Not Used";
		break;
	case 128:
		StepAlias = "Not Used";
		break;
	case 129:
		StepAlias = "Not Used";
		break;
	case 130:
		StepAlias = "Not Used";
		break;
	case 131:
		StepAlias = "High Vacuum Devices Locked";
		break;
	case 132:
		StepAlias = "High Vacuum Pump Starting";
		break;
	case 133:
		StepAlias = "High Vacuum Pump Accelerating";
		break;
	case 134:
		StepAlias = "High Vacuum Pumping Delay";
		break;
	case 135:
		StepAlias = "High Vacuum Pump Error";
		break;
	case 136:
		StepAlias = "Not Used";
		break;
	case 137:
		StepAlias = "Not Used";
		break;
	case 138:
		StepAlias = "Not Used";
		break;
	case 139:
		StepAlias = "Not Used";
		break;
	case 140:
		StepAlias = "Not Used";
		break;
	case 141:
		StepAlias = "High Vacuum Devices Locked";
		break;
	case 142:
		StepAlias = "High Vacuum Pump Starting";
		break;
	case 143:
		StepAlias = "High Vacuum Pump Accelerating";
		break;
	case 144:
		StepAlias = "High Vacuum Pumping Delay";
		break;
	case 145:
		StepAlias = "High Vacuum Pump Error";
		break;
	case 146:
		StepAlias = "Not Used";
		break;
	case 147:
		StepAlias = "Not Used";
		break;
	case 148:
		StepAlias = "Not Used";
		break;
	case 149:
		StepAlias = "Not Used";
		break;
	case 150:
		StepAlias = "Not Used";
		break;
	case 151:
		StepAlias = "High Vacuum Devices Locked";
		break;
	case 152:
		StepAlias = "High Vacuum Pump Starting";
		break;
	case 153:
		StepAlias = "High Vacuum Pump Accelerating";
		break;
	case 154:
		StepAlias = "High Vacuum Pumping Delay";
		break;
	case 155:
		StepAlias = "High Vacuum Pump Error";
		break;
	case 156:
		StepAlias = "Not Used";
		break;
	case 157:
		StepAlias = "Not Used";
		break;
	case 158:
		StepAlias = "Not Used";
		break;
	case 159:
		StepAlias = "Not Used";
		break;
	case 160:
		StepAlias = "Not Used";
		break;
	case 161:
		StepAlias = "High Vacuum Pumping Transition";
		break;
	case 162:
		StepAlias = "High Vacuum Pumping Transition (Nominal)";
		break;
	case 163:
		StepAlias = "Not Used";
		break;
	case 164:
		StepAlias = "Not Used";
		break;
	case 165:
		StepAlias = "Not Used";
		break;
	case 166:
		StepAlias = "Not Used";
		break;
	case 167:
		StepAlias = "Not Used";
		break;
	case 168:
		StepAlias = "Not Used";
		break;
	case 169:
		StepAlias = "Not Used Yet - You should not be there...";
		break;
	case 170:
		StepAlias = "Not Used Yet - You should not be there...";
		break;
	case 171:
		StepAlias = "Not Used Yet - You should not be there...";
		break;
	case 172:
		StepAlias = "Not Used Yet - You should not be there...";
		break;
	case 173:
		StepAlias = "Not Used Yet - You should not be there...";
		break;
	case 174:
		StepAlias = "Not Used Yet - You should not be there...";
		break;
	case 175:
		StepAlias = "Not Used Yet - You should not be there...";
		break;
	case 176:
		StepAlias = "Not Used Yet - You should not be there...";
		break;
	case 400:
		StepAlias = "Valve's Interlocks Overriding";
		break;
	case 401:
		StepAlias = "Pumping Low Vacuum";
		break;

	case 0:
		break;
	default:
		StepAlias = "Step Code: " + PVUtil.getString(pvs[1]);
		break;
}
Logger.info(pvs[1] + " Step code: " + StepCode + " alias: " + StepAlias);

try {
	widget.setPropertyValue("tooltip", StepAlias);
} catch (err) {
	Logger.warning(err);
}
