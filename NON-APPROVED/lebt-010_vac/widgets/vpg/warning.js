PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var warningMsg  = "";
var warningCode = 0;

if (PVUtil.getLong(pvs[0]))
	warningCode = PVUtil.getLong(pvs[1]);

switch (warningCode) {
	case 99:
		warningMsg = "Service Mode (Leak Detection) Over Time";
		break;
	case 19:
		warningMsg = "High Vacuum Pumping Starting Not Possible - All Systems are Locked";
		break;
	case 18:
		warningMsg = "1st High Vacuum Pumping System Error - Angle Valve Position Error";
		break;
	case 17:
		warningMsg = "2nd High Vacuum Pumping System Error - Angle Valve Position Error";
		break;
	case 16:
		warningMsg = "3rd High Vacuum Pumping System Error - Angle Valve Position Error";
		break;
	case 15:
		warningMsg = "4th High Vacuum Pumping System Error - Angle Valve Position Error";
		break;
	case 14:
		warningMsg = "5th High Vacuum Pumping System Error - Angle Valve Position Error";
		break;
	case 11:
		warningMsg = "Atmospheric & Vacuum Starting are Enabled";
		break;
	case 10:
		warningMsg = "No Starting Enabled";
		break;
	case 9:
		warningMsg = "Atmospheric Starting is Enabled";
		break;
	case 3:
		warningMsg = "Automatic Restart On-going";
		break;
	case 2:
		warningMsg = "All Pumping Systems Locked - Starting Not Possible";
		break;
	case 1:
		warningMsg = "No Mode Selected";
		break;

	case 0:
		break;
	default:
		warningMsg = "Warning Code: " + PVUtil.getString(pvs[1]);
		break;
}
Logger.info(pvs[1] + " Warning code: " + warningCode + " message: " + warningMsg);

try {
	pvs[2].setValue(warningMsg);
} catch (err) {
	Logger.warning(err);
}
