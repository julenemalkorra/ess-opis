importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var ORANGE = ColorFontUtil.getColorFromRGB(255,255,0);
var GREY = ColorFontUtil.getColorFromRGB(67,63,61);

var value = PVUtil.getLong(pvs[0]);
//ConsoleUtil.writeInfo("value: " + value);

if (value == 1) {
	widget.setPropertyValue("background_color",ORANGE);
} else {
	widget.setPropertyValue("background_color", GREY);
}
